# KOTLIN Research Topic #

Michael Wright - CIS2250

The TicketOrder class was re-written in Kotlin. This project works as a standalone, but if you would like to copy the file over to your own project you will need to update the settings.gradle file as shown in the following link:
https://github.com/realm/realm-java/issues/7374#issuecomment-809267410