package info.hccis.performancehall_mobileappbasicactivity.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder
import java.io.Serializable

/**
 * Class brought from the Performance Hall web application.  It also has some additional methods
 * related to Ticket Orders.  It's attributes correspond to the attributes which are found in
 * the database.
 *
 * @author bjmaclean
 * @since 20220202
 */
@Entity(tableName = "TicketOrder")
class TicketOrder : Serializable {
    //Note these match the field names from the database (and keys in json)
    @PrimaryKey(autoGenerate = true)
    var id = 0
    var numberOfTickets = 0
    var costOfTickets = 0.0
    var hollpassNumber = 0

    constructor() {}
    constructor(id: Int) {
        this.id = id
    }

    /**
     * Calculate the cost for a ticket order
     *
     * @return cost
     * @since 20220105
     * @author cis2250
     */
    fun calculateTicketPrice(): Double {
        var discount = 0.0
        if (numberOfTickets >= 20) {
            discount += DISCOUNT_VOLUME_20
        } else if (numberOfTickets >= 10) {
            discount += DISCOUNT_VOLUME_10
        }
        if (validateHollPassNumber()) {
            discount += DISCOUNT_HOLLPASS
        }
        costOfTickets = numberOfTickets * COST_TICKET * (1 - discount)
        return costOfTickets
    }

    /**
     * Verify if a HollPassNumber if correct
     * Rules:  Length is 5 and a multiple of 13
     *
     * @return valid result
     * @since 20220105
     * @author cis2250
     */
    fun validateHollPassNumber(): Boolean {
        var valid = false
        if (hollpassNumber >= 10000 && hollpassNumber < 100000) {
            if (hollpassNumber % 13 == 0) {
                valid = true
            }
        }
        return valid
    }

    /**
     * Return if a valid number of tickets or not
     * @return true if valid
     * @since 20220117
     * @author BJM
     */
    fun validateNumberOfTickets(): Boolean {
        return if (numberOfTickets > 0 && numberOfTickets <= MAX_TICKETS) {
            true
        } else {
            false
        }
    }

    override fun toString(): String {
        return ("Ticket Order" + System.lineSeparator()
                + "HollPass Number=" + hollpassNumber + System.lineSeparator()
                + "Number of Tickets=" + numberOfTickets + System.lineSeparator()
                + "Order cost: $" + calculateTicketPrice())
    }

    companion object {
        const val MAX_TICKETS = 100.0
        const val COST_TICKET = 10.0
        const val DISCOUNT_HOLLPASS = 0.1
        const val DISCOUNT_VOLUME_10 = 0.1
        const val DISCOUNT_VOLUME_20 = 0.15
    }
}