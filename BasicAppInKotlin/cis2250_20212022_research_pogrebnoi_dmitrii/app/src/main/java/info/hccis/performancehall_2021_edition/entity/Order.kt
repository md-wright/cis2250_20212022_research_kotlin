package info.hccis.performancehall_2021_edition.entity

/**
 * Data class for CIS-2250 Research assignment
 *
 * @author Dmitrii Pogrebnoi
 * @since 2022-03-15
 */

data class Order(
    var name: String = "",
    var numberOfTickets: Int = 0,
    var hasHollpass: Boolean = false,
    var cost: Double = 0.0
)