package info.hccis.performancehall_2021_edition

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.TextView
import info.hccis.performancehall_2021_edition.entity.Order
import java.text.NumberFormat
import kotlin.math.roundToInt

/**
 * Main (and only) Activity for CIS-2250 Research assignment
 * Kotlin version of CIS-2232 Performance Hall console application
 *
 * @author Dmitrii Pogrebnoi
 * @since 2022-03-15
 */

// Class extends and implements with only colon. Simplifier syntax.
class MainActivity : AppCompatActivity(), View.OnClickListener {

    // Constants
    // Because of null safe we should "promise" to assign value later
    // Type of variable after colons
    // New line replaces semicolon.
    lateinit var txtOutput: TextView
    lateinit var txtInput: TextView
    lateinit var currentOrder: Order
    // No  variable type needed while assign
    // val declaration means variable will not changed
    val TICKET_PRICE = 10;
    val DISCOUNT_HOLLPASS = 10;
    val DISCOUNT_BUNDLE_QTY = mapOf(0 to 10, 1 to 20);
    val DISCOUNT_BUNDLE_VAL = mapOf(0 to 10, 1 to 15);
    val orders = ArrayList<Order>()
    // var declaration allow to change variable value
    var appStage = 0
    var orderStage = 0

    // We will have fun with functions
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        txtOutput = findViewById(R.id.txtOutput)
        txtInput = findViewById(R.id.txtInput)

        // We can use semicolon to put multiple expressions on same line
        // Sadly, we can't assign multiple variables like a = b = ""
        txtOutput.text = ""; txtInput.text = ""

        txtInput.setOnClickListener(this)

        displayMenu()
    }

    // Question mark if we're sure to accept null values
    override fun onClick(view: View?) {
        if(txtInput.text.isNotBlank()) {
            val input = txtInput.text.toString()
            txtOutput.append(System.lineSeparator() + input)
            Log.v(getString(R.string.console_tag), input)
            txtInput.text = ""
            // "Switch" expression. No break keyword required.
            when(appStage) {
                0 -> selectMenu(input.toInt())
                1 -> displayOrderRequest(input)
            }
        }
    }

    /**
     * Display menu
     * Menu data takes from the strings.xml resource
     * The Input switches to only char number type
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    private fun displayMenu() {
        txtInput.inputType = InputType.TYPE_CLASS_NUMBER
        txtInput.filters = arrayOf(InputFilter.LengthFilter(1))
        console(getString(R.string.business_name))
        console(getString(R.string.menu_line1))
        console(getString(R.string.menu_line2))
        console(getString(R.string.menu_line3))
        console(getString(R.string.input_request))
    }

    /**
     * In Menu input handler
     * @param option is entered number
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    private fun selectMenu(option: Int) {
        when(option) {
            0 -> {
                console("Goodbye")
                finishAndRemoveTask()
                System.exit(0)
            }
            1 -> {
                appStage = 1
                orderStage = 0
                displayOrderRequest()
            }
            2 -> {
                displaySummary()
                displayMenu()
            }
            else -> {
                console(getString(R.string.err_option_menu), true)
                displayMenu()
            }
        }
    }

    /**
     * Entering order input handler
     * @param input is entered text in the input
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    // Method with default parameter
    private fun displayOrderRequest(input: String = "") {
        // Easy to use variables in strings
        var request = ""
        when(orderStage) {
            0 -> {
                request = "Enter name:"
                txtInput.inputType = InputType.TYPE_CLASS_TEXT
                txtInput.filters = arrayOf(InputFilter.LengthFilter(25))
                currentOrder = Order()
                orderStage = 1
            }
            1 -> {
                currentOrder.name = input
                request = "Enter number of tickets:"
                txtInput.inputType = InputType.TYPE_CLASS_NUMBER
                txtInput.filters = arrayOf(InputFilter.LengthFilter(3))
                orderStage = 2
            }
            2-> {
                currentOrder.numberOfTickets = input.toInt()
                orderStage = 3
                displayOrderRequest()
                return
            }
            3 -> {
                request = "Do you have a HollPass? (y/n)"
                txtInput.inputType = InputType.TYPE_CLASS_TEXT
                txtInput.filters = arrayOf(InputFilter.LengthFilter(1))
                orderStage = 4
            }
            4 -> {
                if(input.matches("[YyNn]".toRegex())) {
                    // Equals
                    currentOrder.hasHollpass = input.equals("y", true)
                    // Inline if-else
                    orderStage = if(currentOrder.hasHollpass) 5 else 7
                } else {
                    orderStage = 3
                    console(getString(R.string.err_option_haspass), true)
                }
                displayOrderRequest()
                return
            }
            5 -> {
                request = "Please enter your HollPass number:"
                txtInput.inputType = InputType.TYPE_CLASS_NUMBER
                txtInput.filters = arrayOf(InputFilter.LengthFilter(7))
                orderStage = 6
            }
            6 -> {
                if(input.length == 7 && input.toInt() % 13 == 0) {
                    orderStage = 7
                } else {
                    orderStage = 5
                    console(getString(R.string.err_option_hollpass), true)
                }
                displayOrderRequest()
                return
            }
            7 -> {
                currentOrder.cost = this.calculateCost(currentOrder.numberOfTickets, currentOrder.hasHollpass);
                orders.add(currentOrder)
                displayOrder(currentOrder)
                appStage = 0
                orderStage = 0
                displayMenu()
                return
            }
        }
        console(request)
        console(getString(R.string.input_request))
    }

    /**
     * Calculate and display summary for all orders
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    private fun displaySummary() {
        var sales = 0.0
        var tickets = 0
        // ForEach. Very simple usage
        orders.forEach {
            sales += it.cost
            tickets += it.numberOfTickets
        }
        console("Summary")
        console("Total sales: ${NumberFormat.getCurrencyInstance().format(sales)}")
        console("Tickets sold: $tickets")
        console("Average cost: ${NumberFormat.getCurrencyInstance().format(if(tickets > 0) sales / tickets else 0)} per ticket", true)
    }

    /**
     * Display summary for given order
     * @param order is Order object which need to processed
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    private fun displayOrder(order: Order) {
        txtOutput.append(System.lineSeparator())
        console("Thank you for your order!")
        // Easy to concatenating variables and objects with strings
        console("Name: ${order.name}")
        console("Number of tickets: ${order.numberOfTickets}")
        console("Regular cost: ${NumberFormat.getCurrencyInstance().format(order.numberOfTickets * TICKET_PRICE)}")
        console("Discount: ${calculateDiscount(order.numberOfTickets, order.hasHollpass).roundToInt()}%")
        console("Cost: ${NumberFormat.getCurrencyInstance().format(order.cost)}", true)
    }

    /**
     * Calculate and return cost of the order
     * @param tickets how many tickets in the order
     * @param hollpass is Hollpass provided
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    private fun calculateCost(tickets: Int, hollpass: Boolean): Double {
        var cost = tickets * TICKET_PRICE
        return cost - (cost * calculateDiscount(tickets, hollpass) / 100)
    }

    /**
     * Calculate and return discount for the order
     * @param tickets how many tickets in the order
     * @param hollpass is Hollpass provided
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    private fun calculateDiscount(tickets: Int, hollpass: Boolean): Double {
        var discount = 0.0
        discount += if(hollpass) DISCOUNT_HOLLPASS else 0
        // My favorite! Reverse switch... I mean WHEN clause
        when(true) {
            // We can use unsafe access (!!) to variable if we're sure there is no null error happens
            (tickets >= DISCOUNT_BUNDLE_QTY[1]!!) -> discount += DISCOUNT_BUNDLE_VAL[1]!!
            (tickets >= DISCOUNT_BUNDLE_QTY[0]!!) -> discount += DISCOUNT_BUNDLE_VAL[0]!!
        }
        return discount
    }

    /**
     * Populate display and log outputs
     * @param text displayed message
     * @param extraLine if extra line needed after message
     *
     * @author Dmitrii Pogrebnoi
     * @since 2022-03-15
     */
    private fun console(text: String, extraLine: Boolean = false) {
        txtOutput.append(System.lineSeparator() + text + if(extraLine) System.lineSeparator() else "")
        Log.v(getString(R.string.console_tag), text)
    }

}